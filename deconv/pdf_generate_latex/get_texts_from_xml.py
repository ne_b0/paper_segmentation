import os
from lxml import etree
from xml.dom import minidom

text_folder = '/media/natasha/Data1/data_pieces/text/'
text_file = os.path.join(text_folder, 'napwiki-20180901-pages-articles.xml')
list_text = '/media/natasha/Data1/data_pieces/text/list_text_from_xml.txt'

with open(text_file, 'r') as fin:
    data = fin.read()

xmldoc = minidom.parse(text_file)
itemlist = xmldoc.getElementsByTagName('text')
print(len(itemlist))

texts = []
names = []
for i, s in enumerate(itemlist):
    try:
        if len(s.firstChild.nodeValue) > 600:
            texts.append(s.firstChild.nodeValue)
            names.append('text%d.txt' % i)

    except:
        print('empty')

for i, text in enumerate(texts):
    with open(os.path.join(text_folder, names[i]), 'w') as fout:
        fout.write(text)

with open(list_text, 'w') as fout:
    fout.write('*'.join(names))
