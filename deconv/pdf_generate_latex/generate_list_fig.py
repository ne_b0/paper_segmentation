import os

fig_folder = '/media/natasha/Data1/data_pieces/figures/val2014'
list_fig = '/media/natasha/Data1/data_pieces/figures/list_fig.txt'

names = os.listdir(fig_folder)
names = ['val2014/' + name for name in names]
with open(list_fig, 'w') as fout:
    fout.write('*'.join(names))
