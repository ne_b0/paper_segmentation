import multiprocessing
import subprocess


def work(cmd):
    return subprocess.call(cmd, shell=True, executable="/bin/bash")


if __name__ == '__main__':
    count = multiprocessing.cpu_count()
    print('count ', count)
    pool = multiprocessing.Pool(processes=count)

    cmds = []
    num = 10000
    for i in range(count):
        cmds.append('./run.sh {} template_batch9_thread{}_'.format(num, i))
    print('cmds ', cmds)
    print(pool.map(work, cmds))
