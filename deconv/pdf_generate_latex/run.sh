# arguments
Num=$1

Prefix=$2 # template_batch6
Projectdir='/home/natasha/Downloads/paper-segmentation/deconv/deconv/pdf_generate_latex'

cd '/media/natasha/Data1/data_pieces/generated_dataset'

# generate tex files
python $Projectdir/main.py --pageNum $Num --prefix $Prefix

# generate pdf files
for ((it=0; it<Num; it++))
do
    if [ -f $Prefix$it.tex ]; then
        pdflatex -interaction=nonstopmode $Prefix$it.tex
    fi
done

# convert to image
for ((it=0; it<Num; it++))
do
    echo $it
    if [ -f $Prefix$it.pdf ]; then
        convert -density 300 $Prefix$it.pdf -filter lagrange -distort resize 33% -background white -alpha remove -quality 100 $Prefix$it.jpg
    fi
done

# visualize
python $Projectdir/generate_xml.py -I "$Prefix"*.jpg --visualize
